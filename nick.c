// Exercise 5.9 Solution 
#include <stdio.h>
#include <math.h>

double calculateCharges(double hours); // function prototype 

int main()
{ 
   printf("%s", "Enter the hours parked for 3 cars: ");
   int first = 1; // flag for printing table headers 
   double totalCharges = 0.0; // total charges for all cars 
   double totalHours = 0.0; // total number of hours for all cars 

   // loop 3 times for 3 cars 
   for (unsigned int i = 1; i <= 3; ++i) { 
      double h; // number of hours for current car 
      scanf("%lf", &h);
      totalHours += h; // add current hours to total hours 
   
      // if first time through loop, display headers 
      if (first) { 
         printf("%5s%15s%15s\n", "Car", "Hours", "Charge");

         // set flag to false to prevent from printing again 
         first = 0; 
      }  
   
      // calculate current car's charge and update total 
      double currentCharge; // parking charge for current car 
      totalCharges += (currentCharge = calculateCharges(h));

      // display row data for current car 
      printf("%5d%15.1f%15.2f\n", i, h, currentCharge);
   }  

   // display row data for totals 
   printf("%5s%15.1f%15.2f\n", "TOTAL", totalHours, totalCharges);
}  

// calculateCharges returns charge according to number of hours 
double calculateCharges(double hours)
{ 
   double charge; // calculated charge 

   // $2 for up to 3 hours 
   if (hours < 3.0) {
      charge = 2.00;
   }  

   // $.50 for each hour or part thereof in excess of 3 hours 
   else if (hours < 19.0) {
      charge = 2.00 + .50 * ceil(hours - 3.0);
   }  

   else { // maximum charge $10 
      charge = 10.0;
   }  
   
   return charge; // return calculated charge 
} 